FROM sujanassg/alpine-java:latest

COPY [ "target/*.jar", "/opt/" ]

WORKDIR "/opt/"

CMD [ "java", "-jar", "assignment-0.0.1-SNAPSHOT.jar" ]



